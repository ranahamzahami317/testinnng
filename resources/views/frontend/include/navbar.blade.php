<nav class="navbar navbar-expand-lg  navbar-dark bg-dark" style="background-color:#333 !important; box-shadow:         inset 0 0 10px #000000  !important;">
         <a class="navbar-brand" href="{{ route('home') }}" style="padding-left:50px">Tech Net </a>
         <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
         <span class="navbar-toggler-icon"></span>
         </button>
         <form class="form-inline my-2 my-lg-0 navbar-form">
            <div id="igs" class="input-group input-group-search mx-auto" >
               
               <input type="search" class="form-control" placeholder="Search..." aria-label="Search" aria-describedby="search-button-addon">
               <div class="input-group-append">
                   <button style="  border-radius: 5px;
    background: white;
    color: #e6be1e;padding-right:22px;padding-left:22px;align:center" type="submit"><i style="color: #e6be1e" class="fa fa-search"></i></button>

               </div>
            </div>
         </form>
         <div class="collapse navbar-collapse justify-content-end " id="navbarSupportedContent">
            <ul class="navbar-nav ">
			
 
               <li class="nav-item ">
                  <a class="nav-link text-nowrap" href="#">Download</a>
               </li>
               
			   <li class="nav-item">
                  <a class="nav-link text-nowrap" data-toggle="modal" data-target="#myModal" href="{{ route('log') }}">Login</a>
				  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content" style="    width: 60%;margin-left:10% ">
        <div class="modal-header">
          <h2 class="d-flex mx-auto" style="color: #e6be1e">Signin </h2>
            <button type="button"  class="close" data-dismiss="modal"><small><i class="fa fa-times" aria-hidden="true"></i></small></button>
          </div>
        <div class="modal-body">
        <form method="post" action='' name="login_form">
              <p><input style="    width: 100%;
    border-radius: 40px;padding-left:10px;" type="text" onfocus="this.placeholder=''" onblur="this.placeholder='Email'" class="span3" name="eid" id="email" placeholder="Email"></p>
              <p><input style="    width: 100%;
    border-radius: 40px;padding-left:10px;" type="password" class="span3" name="passwd" placeholder="Password"></p>
      <label><input type="checkbox" name=""> Remember Me</label> <br>
              <p><button type="submit" id="sup" class="btn btn-primary">Sign in</button>
                <a href="#">Forgot Password?</a>
              </p>
            </form>
        </div>

      </div>
      
    </div>
  </div>
				
               </li> 
               <li class="nav-item dropdown">
                  <a class="nav-link  text-nowrap" href="{{ route('Reg') }}"    >
                  Sign Up
                  </a>
                  
               </li>
               
            </ul>
         </div>
      </nav>
