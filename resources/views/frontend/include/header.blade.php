<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8" >
  <title>TECH NET</title>

    <link rel="stylesheet" href="{{asset("Bootstrap/bootstrap.min.css")}}" type="text/css"  />


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


	<style>

.action-lage{
    position: relative;
    width:100%;

}
.action-lage:before{
    position: absolute;
    content: "";
    width: 0;
    height: 0;
    border-left: 15px solid transparent;
    border-right: 15px solid transparent;
    border-top: 20px solid #e6be1e;
    left:50%;
    top:0%;
    z-index: 3;
    margin-left:-15px;

}
 .navbar-form {

	 flex-basis: 100%;
	 margin-left:50px;
	 
 }
@media(min-width:576px) {
    .navbar-expand-sm .input-group-search {
        width: 100% !important;
    }
}
 @media(min-width:768px) {
	 .navbar-expand-md .input-group-search {
		width: 90% !important; 
	 }
 }
 @media(min-width:992px) {
	 .navbar-expand-lg .input-group-search {
		width: 80% !important;
	 }
	 
 }
 @media(min-width:1200px) {
	 .navbar-expand-xl .input-group-search {
		width: 70% !important; 
	 }
 }
 
 
.navbar-collapse .navbar-nav>li>a:hover {
color:#e6be1e !important;
}
.container .row .col>a:hover{
color:white !important;
text-decoration:none;
}
#sup:hover{
background-color:#e6be1e !important;
}
#sup{
	
	background-color:#A9A9A9 !important;
	border:none;
}

.col-md-1  a {
color:#585858;
font-size:30px;
}
.col-md-1>a:hover {
color:#e6be1e;
}	

#nu{
color:black;
}
#nu:hover{
color:#e6be1e;
text-decoration:none;
}
.fa{
color:#585858;
}
.fa:hover{
color:#e6be1e;
}
.fctrl{

    display: inline-flex !important;
    width: 60% !important;
    border-radius: 50px;
}
#as:hover{
    background-color: #e6be1e !important;
    color: white !important;
    border:none !important;
}
#aas:hover{
    background-color: #4A4848 !important;
    color: white !important;
    border:none !important;
}
body{
	
	overflow-x:hidden;
}
.fa-times:hover{
    color: blue !important;

}
.close:focus{
  
    outline: none;
}
</style>
 
 <script language="javascript">

	populateCountries("country2");
</script>
</head>

<body>
