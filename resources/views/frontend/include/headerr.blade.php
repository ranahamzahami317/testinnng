<!DOCTYPE html>
<html lang="en" >
<head>
    <meta charset="UTF-8">
    <title>TECH NET</title>


    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="{{asset("fonts/themify-icons/themify-icons.css")}}">
    <!-- Bootstrap-Core-CSS -->
    <link rel="stylesheet" href="{{asset("css/style.css")}}" type="text/css"  />

    <!-- Style-CSS -->


    <style>
        .action-lage{
            position: relative;
            width:100%;

        }
        .action-lage:before{
            position: absolute;
            content: "";
            width: 0;
            height: 0;
            border-left: 15px solid transparent;
            border-right: 15px solid transparent;
            border-top: 20px solid #e6be1e;
            left:50%;
            top:0%;
            z-index: 3;
            margin-left:-15px;

        }
        .navbar-form {

            flex-basis: 50%;
            margin-left:50px;

        }
        @media(min-width:576px) {
            .navbar-expand-sm .input-group-search {
                width: 100% !important;
            }
        }
        @media(min-width:768px) {
            .navbar-expand-md .input-group-search {
                width: 90% !important;
            }
        }
        @media(min-width:992px) {
            .navbar-expand-lg .input-group-search {
                width: 100% !important;
            }

        }
        @media(min-width:1200px) {
            .navbar-expand-xl .input-group-search {
                width: 70% !important;
            }
        }

        @media (min-width: 1200px) {
            .fg {
                max-width: 100%;
            }
        }


        .navbar-collapse .navbar-nav>li>a:hover {
            color:#e6be1e !important;
        }
        .container .row .col>a:hover{
            color:white !important;
            text-decoration:none;
        }
        #sup:hover{
            background-color:#e6be1e !important;
        }
        #sup{

            background-color:#A9A9A9;
            border:none
        }
        body{

            overflow-x:hidden;
        }
    </style>

</head>

<body>
