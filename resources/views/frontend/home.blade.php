@extends('frontend.layouts.app')
@section('content')
 @if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status') }}
    </div>
@endif
<div id="app" style="padding-right:0px !important; padding-left:0px !important" class="container-fluid" >
    <div id="carouselExampleIndicators"  data-interval="3000" data-pause="hover" style="padding-top:10px;" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="{{ asset('assets/images/up.jpg') }}" alt="First slide">
      <div class="carousel-caption d-none d-md-block">
    <h5>Welcome to Our Hospitality</h5>

  </div>
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="{{ asset('assets/images/up1.jpg') }}" alt="Second slide">
       <div class="carousel-caption d-none d-md-block">
    <h5>Welcome to Our Hospitality</h5>

  </div>
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="{{ asset('assets/images/up3.jpg') }}" alt="Third slide">
       <div class="carousel-caption d-none d-md-block">
    <h5>Welcome to Our Hospitality</h5>

  </div>
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
@endsection
