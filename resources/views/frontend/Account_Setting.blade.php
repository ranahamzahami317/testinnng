@extends('frontend.layouts.app')
@section('content')

    <div class="container mt-5" >

        <div class="form-row ">
            <h1 class="mx-auto"> Account Setting</h1>
        </div>
        <form>
            <div class="form-row mt-3">
                <div class="form-group col-md-6">
                    <label for="inputEmail4">First Name</label>
                    <input style="margin-left: 5px" type="email" class="form-control fctrl" id="inputEmail4" placeholder="First Name">
                </div>
                <div class="form-group col-md-6">
                    <label for="inputPassword4">Last Name</label>
                    <input style="margin-left: 15px" type="text" class="form-control fctrl"  placeholder="Last Name">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Mobile No.</label>
                    <input type="email" class="form-control fctrl" id="inputEmail4" placeholder="+xx-xxx-xxxxxxx">
                </div>
                <div class="form-group col-md-6">
                    <label >Email</label>
                    <input style="margin-left: 45px" type="email" class="form-control fctrl"  placeholder="Email">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Country</label>
                    <input style="margin-left: 15px" type="email" class="form-control fctrl" id="inputEmail4" placeholder="pakistan">
                </div>
                <div class="form-group col-md-6">
                    <label>DOB</label>
                    <input style="margin-left: 45px" type="Date" class="form-control fctrl"  placeholder="11-01-1965">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="inputEmail4">Gender</label>
                    <input style="margin-left: 15px" type="email" class="form-control fctrl" id="inputEmail4" placeholder="Male/Female">
                </div>
                <div class="form-group col-md-6">
                    <label >Username</label>
                    <input style="margin-left: 15px" type="text" class="form-control fctrl" placeholder="john321">
                </div>
            </div>
        </form>
        <div class="form-row">
            <button type="submit" style="border-radius: 15px;background-color: white;color: black;border:1px solid black" id="as" class="btn btn-primary mx-auto  " data-toggle="modal" data-target="#myModalll">Change Password</button>
        </div>
        <!-- Modal -->
        <div id="myModalll" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">

                        <h4 class="modal-title mx-auto">Change Password</h4>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label >Current Password</label>
                                    <input style="margin-left: 12px;margin-top: 15px" type="password" class="form-control fctrl" placeholder="Current Password">
                                </div>

                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="inputEmail4">New Password</label>
                                    <input style="margin-left: 25px;margin-top: 15px" type="password" class="form-control fctrl" id="inputEmail4" placeholder="New Password " >
                                </div>

                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label >Confirm Password</label>
                                    <input style="margin-left: 5px;margin-top: 15px" type="password" class="form-control fctrl" id="inputEmail4" placeholder="Confirm Password">
                                </div>

                            </div>
                            <div class="form-row" style="margin-left: 120px !important">
                                <div class="form-group col-md-12">

                                    <label><input type="checkbox" class="mx-auto justify-content-center" name=""> Log me out from other device</label>    </div>

                            </div>
                        </form>

                    </div>
                    <div class="modal-footer">
                        <div class="btn-group float-right">
                            <button type="button" style="margin-right: 5px;border-radius: 15px;background-color: white;color: black;border:1px solid black;padding-left: 40px;padding-right: 40px;"" id="aas" class="btn btn-primary">Cancel</button>

                            <button type="button" style="border-radius: 15px;background-color: white;color: black;border:1px solid black;padding-left: 50px;padding-right: 50px;" id="as" class="btn btn-primary">Ok</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="container" style="margin-right: 255px;margin-bottom: 200px">

        <div class="btn-group float-right">
            <button type="button" style="margin-right: 5px;border-radius: 15px;background-color: white;color: black;border:1px solid black" id="aas" class="btn btn-primary">Reset Setting</button>

            <button type="button" style="border-radius: 15px;background-color: white;color: black;border:1px solid black" id="as" class="btn btn-primary" data-toggle="modal" data-target="#myModall" >Save Changes</button>
            <div id="myModall" class="modal fade" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">

                            <h4 class="modal-title mx-auto">Save Changes</h4>
                        </div>
                        <div class="modal-body">
                            <form>
                                <div class="form-row" style="padding-left: 90px !important">
                                    <div class="form-group col-md-12">
                                        <h3 style="font-size: 0.75rem !important"> Please Enter the Current Password to Save Changes</h3>

                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <label >Password</label>
                                            <input style="margin-top: 15px;width: 70% !important" type="password" class="form-control fctrl" id="inputEmail4" placeholder="Current Password">
                                        </div>

                                    </div>
                                </div>
                            </form>

                        </div>
                        <div class="modal-footer">
                            <div class="btn-group float-right">
                                <button type="button" style="margin-right: 5px;border-radius: 15px;background-color: white;color: black;border:1px solid black;padding-left: 40px;padding-right: 40px;"" id="aas" class="btn btn-primary">Cancel</button>

                                <button type="button" style="border-radius: 15px;background-color: white;color: black;border:1px solid black;padding-left: 50px;padding-right: 50px;" id="as" class="btn btn-primary">Ok</button>
                            </div>
                        </div>

                    </div>
                </div>


            </div>
        </div>
    </div>



@endsection