@extends('frontend.layouts.app')
@section('content')
@if(isset($status))
<div class="row">
    <div class="col-md-12" >
      <div class="alert alert-success" style="width: 50%;margin-left: 23%;">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong>Success!</strong> {{ $status}}
      </div>
    </div>
  </div>  
@endif
@if(isset($warning))
<div class="row">
    <div class="col-md-12" >
      <div class="alert alert-warning" style="width: 50%;margin-left: 23%;">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong>Warning!</strong> {{ $warning}}
      </div>
    </div>
  </div>  
@endif
<div style="margin:30px 100px 280px 300px"  class="container" >
<form method="POST" action="{{ route('login') }}">
  @csrf
  <div class="form-row" >
    <div class="form-group col-md-offset-6 col-md-6 ">
      <label for="inputEmail4">User Name</label>
      <input id="inputEmail4" type="email" class="form-control d-inline-flex {{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Email" name="email" value="{{ old('email') }}" required autofocus>

      @if ($errors->has('email'))
          <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('email') }}</strong>
          </span>
      @endif
    </div>
    </div>
   <div class="form-row">
    <div class="form-group col-md-offset-6 col-md-6">
      <label for="inputPassword4">Password</label>
      <input id="inputPassword4" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

      @if ($errors->has('password'))
          <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('password') }}</strong>
          </span>
      @endif
    </div>
  </div>
   
  
  <button id="sup"  type="submit" class="btn btn-primary">Sign in</button>
  

  </form>
<!-- Default form register -->
  
  </div>

@endsection
