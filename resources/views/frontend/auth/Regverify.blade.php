@extends('frontend.layouts.app')
@section('content')
<div   class="container justify-content-center mt-5" >
<form class="justify-content-center" method="POST" action="{{ url('VerifyUser') }}" >
@csrf  
  <div class="form-row justify-content-center" >
    <div class="form-group col-md-3 col-md-offset-9 ">
      <label >Verify Account</label>
	    <input type="text" class="form-control " placeholder="Paste Code we sent to you" name="token">
      <button type="submit" id="sup" class="btn btn-primary mt-3">Verify</button>
   </div>
	 </div>
</form>
</div>
@endsection