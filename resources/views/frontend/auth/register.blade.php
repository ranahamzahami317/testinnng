@extends('frontend.layouts.app')
@section('content')

    <div   class="container d-flex justify-content-center mt-5" >
<form  method="POST" action="{{ route('register') }}">
    <div class="form-row" >
        <div class="form-group col-md-offset-2 col-md-10 ">
            <h2 style="font-size: 25px;font-family: roboto"> Create Your Account Here </h2>
        </div>
    </div>
    @csrf

  <div class="form-row" >
      <div class="form-group col-md-offset-2 col-md-10 ">
      <label for="inputEmail4">Email</label>
       <input id="email" type="email" class="form-control d-inline-flex {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="Email" id="inputEmail4" value="{{ old('email') }}" required>
     @if ($errors->has('email'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('email') }}</strong>
        </span>
    @endif
    </div>
  </div>
   <div class="form-row">
       <div class="form-group col-md-offset-2 col-md-10 ">
      <label for="inputPassword4">Password</label>
      <input id="password" type="password" id="inputPassword4" placeholder="Password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

    @if ($errors->has('password'))
        <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('password') }}</strong>
        </span>
    @endif
    </div>
  </div>
  <div class="form-row">
      <div class="form-group col-md-offset-2 col-md-10 ">
      <label for="inputPassword4"> Confirm Password</label>
      <input type="password" class="form-control" id="inputPassword4" name="password_confirmation" placeholder="Confirm Password">
    </div>
  </div>
   <div class="form-row">
       <div class="form-group col-md-offset-2 col-md-10 ">
      <label for="inputPassword4"> Country</label>
      <select class="form-control" id="inputcountry" placeholder="Country" name="country">
        @if(isset($contury))
        @foreach($contury AS $index => $value)
        <option value="{{ $index }}">{{ $value->country_name }}</option>
        @endforeach
        @endif
      </select>
      @if ($errors->has('confirm'))
          <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('confirm') }}</strong>
          </span>
      @endif
    </div>
  </div>
  
  
  <div class="form-group">
    <div class="form-check">
      <input class="form-check-input" type="checkbox" id="gridCheck" name="confirm" value="1">
      <label class="form-check-label" for="gridCheck">
        By clicking <em>Sign up</em> you agree to our <a href="" target="_blank">terms of service</a>.
      </label>
      @if ($errors->has('confirm'))
          <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first('confirm') }}</strong>
          </span>
      @endif
    </div>
  </div>
  <button id="sup"  type="submit" onclick="location.href = 'login.html';" class="btn btn-primary">Sign Up</button>
</form>
<!-- Default form register -->
  
  </div>
@endsection
