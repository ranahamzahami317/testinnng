
@include('include.header')

@include('include.navbar')

<div   class="container d-flex justify-content-center mt-5" >
<form>
   <div class="form-row" >
    <div class="form-group col-md-offset-2 col-md-10 ">
    <h2 style="font-size: 25px;font-family: roboto"> Create Your Account Here </h2>
    </div>
    </div>
  <div class="form-row" >
    <div class="form-group col-md-offset-2 col-md-10 ">
      <label for="inputEmail4">Email</label>
	 <input type="email" class="form-control d-inline-flex" id="inputEmail4" placeholder="Email">
    </div>
    </div>
	 <div class="form-row">
    <div class="form-group col-md-offset-2 col-md-10">
      <label for="inputPassword4">Password</label>
      <input type="password" class="form-control" id="inputPassword4" placeholder="Password">
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-offset-2 col-md-10">
      <label for="inputPassword4"> Confirm Password</label>
      <input type="password" class="form-control" id="inputPassword4" placeholder="Password">
    </div>
  </div>
   <div class="form-row">
    <div class="form-group col-md-offset-2 col-md-10">
      <label for="inputPassword4"> Country</label>
	  <div style='text-align:center;'>
	  <select id="country2" name ="country2"></select>
	 
    </div>
    </div>
  </div>
  
  
  <div class="form-group">
    <div class="form-check">
      <input class="form-check-input" type="checkbox" id="gridCheck">
      <label class="form-check-label" for="gridCheck">
        By clicking <em>Sign up</em> you agree to our <a href="" target="_blank">terms of service</a>.
      </label>
    </div>
  </div>
  <button type="submit"  id="sup"  class="btn btn-primary">Sign Up</button>
</form>
<!-- Default form register -->
	
	</div>

	@include('include.footer')