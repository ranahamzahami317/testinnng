@extends('frontend.layouts.app2')
@section('content')


    <div class="main" style="padding-top:50px;">

        <div class="container cmd"  >
            <h2> Please Build Your Profile</h2>
            <form method="POST" id="signup-form" class="signup-form">
                <h3>
                    <span class="icon ic"><i class="ti-user"></i></span>
                    <span class="title_text">Personal</span>
                </h3>
                <fieldset>
                    <legend>
                        <span class="step-heading">Personal Informaltion: </span>
                        <span class="step-number">Step 1 / 4</span>
                    </legend>

                    <div class="form-group frm">
                        <label for="first_name" class="form-label required">First name</label>
                        <input type="text" name="first_name" id="first_name" />
                    </div>

                    <div class="form-group">
                        <label for="last_name" class="form-label required">Last name</label>
                        <input type="text" name="last_name" id="last_name" />
                    </div>

                    <div class="form-row frr">
                        <div class="form-date dt" style="width: 310px !important">
                            <label for="birth_date" class="form-label">Date of birth</label>
                            <div class="form-date-group fdg">
                                <div class="form-date-item fdt">
                                    <select id="birth_date" name="birth_date"></select>
                                    <span class="select-icon"><i class="ti-angle-down"></i></span>
                                </div>
                                <div class="form-date-item fdt">
                                    <select id="birth_month" name="birth_month"></select>
                                    <span class="select-icon"><i class="ti-angle-down"></i></span>
                                </div>
                                <div class="form-date-item fdt">
                                    <select id="birth_year" name="birth_year"></select>
                                    <span class="select-icon"><i class="ti-angle-down"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-select">
                            <label for="gender" class="form-label">Gender</label>
                            <div class="select-list" style="margin-bottom: 0px !important">
                                <select name="gender">

                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </select>
                            </div>
                        </div>

                    </div>


                    <div class="form-group">
                        <label for="user_name" class="form-label required">User name</label>
                        <input type="text" name="user_name" id="user_name" />
                    </div>




                </fieldset>

                <h3>
                    <span class="icon ic"><i class="ti-email"></i></span>
                    <span class="title_text">Contact</span>
                </h3>
                <fieldset>
                    <legend>
                        <span class="step-heading">Contact Informaltion: </span>
                        <span class="step-number">Step 2 / 4</span>
                    </legend>
                    <div class="form-group">
                        <label for="email" class="form-label ">Email</label>
                        <input type="email" name="email" disabled id="email" />
                    </div>

                    <div class="form-group">
                        <label for="phone" class="form-label required">Mobile Number</label>
                        <input type="tel" name="phone" id="phone" />
                    </div>

                    <div class="form-group">
                        <label for="address" class="form-label required">Address</label>
                        <input type="text" name="address" id="address" />
                    </div>


                </fieldset>

                <h3>
                    <span class="icon ic"><i class="fa fa-share-square-o " ></i></span>
                    <span class="title_text">Social</span>
                </h3>
                <fieldset>
                    <legend>
                        <span class="step-heading">Social Informaltion: </span>
                        <span class="step-number">Step 3 / 4</span>
                    </legend>
                    <div class="form-group" style="display: flex !important;">
                        <input class="form-control border-secondary py-2" type="search" value="search Your Friends Here">

                        <button class="btn btn-outline-secondary" type="button">
                            <i class="fa fa-search"></i>
                        </button>

                    </div>
                    <div class="form-group " style="font-size: 30px;">
                        <label for="designation" class="form-label required">Social Media Links</label>
                        <label id="designation-error" class="error" for="designation"></label>
                        <div class="d-flex justify-content-start">
                            <i class="fa fa-facebook-official"></i><br>
                            <i class="fa fa-twitter"></i><br>

                            <i class="fa fa-instagram"></i><br>
                            <i class="fa fa-linkedin-square"></i>
                        </div>
                    </div>
                </fieldset>

                <h3>
                    <span class="icon ic"><i class="fa fa-list-alt"></i></span>
                    <span class="title_text">Complete</span>
                </h3>
                <fieldset>
                    <legend>
                        <span class="step-heading">Finish: </span>
                        <span class="step-number">Step 4 / 4</span>
                    </legend>
                    <div>
                        <div class="card text-white bg-secondary mb-3" style="max-width: 22rem;">
                            <div class="card-body">
                                <h4 class="card-title">Welcome To Social Media <i class="fa fa-heart" ></i></h4>
                                <p class="card-text">Now Enjoy Making New Friends.</p>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>


@endsection
