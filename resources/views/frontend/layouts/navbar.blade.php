<nav class="navbar navbar-expand-lg  navbar-dark bg-dark" style="background-color:#333 !important; box-shadow:         inset 0 0 10px #000000  !important;">
         <a class="navbar-brand" href="home" style="padding-left:50px">Tech Net </a>
         <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
         <span class="navbar-toggler-icon"></span>
         </button>
    <form class="form-inline my-2 my-lg-0 navbar-form">
        <div id="igs" class="input-group input-group-search mx-auto" >

            <input type="search" class="form-control" placeholder="Search..." aria-label="Search" aria-describedby="search-button-addon">
            <div class="input-group-append">
                <button style="  border-radius: 5px;
    background: white;
    color: #e6be1e;padding-right:22px;padding-left:22px;align:center" type="submit"><i style="color: #e6be1e" class="fa fa-search"></i></button>

            </div>
        </div>
    </form>
         <div class="collapse navbar-collapse justify-content-end " id="navbarSupportedContent">
            <ul class="navbar-nav ">
            
 
               <li class="nav-item ">
                  <a class="nav-link text-nowrap" href="#">Download</a>
               </li>
               @if (Route::has('login'))
               @auth
               <li class="nav-item">
                  <a class="nav-link text-nowrap" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Logout</a>
                   <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                  </form>

               </li> 
               @else
               <li class="nav-item">
                  <a class="nav-link text-nowrap"  href="{{ route('login') }}">Login</a>
               </li>
               @if (Route::has('register')) 
               <li class="nav-item dropdown">
                  <a class="nav-link  text-nowrap" href="{{ route('register') }}"    >
                  Sign Up
                  </a>

                  
               </li>
               @endif
               @endif
               @endauth
            </ul>
         </div>
      </nav>