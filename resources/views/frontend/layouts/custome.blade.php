<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>


    <link rel="stylesheet" href="{{asset("Bootstrap/bootstrap.min.css")}}" type="text/css"  />


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="{{asset("css/style.css")}}" type="text/css"  />

    <style>

        .action-lage{
            position: relative;
            width:100%;

        }
        .action-lage:before{
            position: absolute;
            content: "";
            width: 0;
            height: 0;
            border-left: 15px solid transparent;
            border-right: 15px solid transparent;
            border-top: 20px solid #e6be1e;
            left:50%;
            top:0%;
            z-index: 3;
            margin-left:-15px;

        }
        .navbar-form {

            flex-basis: 100%;
            margin-left:50px;

        }
        @media(min-width:576px) {
            .navbar-expand-sm .input-group-search {
                width: 100% !important;
            }
        }
        @media(min-width:768px) {
            .navbar-expand-md .input-group-search {
                width: 90% !important;
            }
        }
        @media(min-width:992px) {
            .navbar-expand-lg .input-group-search {
                width: 80% !important;
            }

        }
        @media(min-width:1200px) {
            .navbar-expand-xl .input-group-search {
                width: 70% !important;
            }
        }


        .navbar-collapse .navbar-nav>li>a:hover {
            color:#e6be1e !important;
        }
        .container .row .col>a:hover{
            color:white !important;
            text-decoration:none;
        }
        #sup:hover{
            background-color:#e6be1e !important;
        }
        #sup{

            background-color:#A9A9A9 !important;
            border:none;
        }

        .col-md-1  a {
            color:#585858;
            font-size:30px;
        }
        .col-md-1>a:hover {
            color:#e6be1e;
        }

        #nu{
            color:black;
        }
        #nu:hover{
            color:#e6be1e;
            text-decoration:none;
        }
        .fa{
            color:#585858;
        }
        .fa:hover{
            color:#e6be1e;
        }
        .fctrl{

            display: inline-flex !important;
            width: 60% !important;
            border-radius: 50px;
        }
        #as:hover{
            background-color: #e6be1e !important;
            color: white !important;
            border:none !important;
        }
        #aas:hover{
            background-color: #4A4848 !important;
            color: white !important;
            border:none !important;
        }
        body{

            overflow-x:hidden;
        }
        .fa-times:hover{
            color: blue !important;

        }
        .close:focus{

            outline: none;
        }
    </style>



</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main >
            @yield('content')
        </main>
    </div>
</body>
</html>
